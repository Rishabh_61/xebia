import GlobalConfig from '../globalConfig';

export const userLoginParams = (data = {}) => ({
  method: "GET",
  headers: GlobalConfig.getHeaders(["JSON"]),
  url: GlobalConfig.getApiUrlFromRoot("/people"),
  data
});

export const planetsList = (data = {}) => ({
  method: "GET",
  headers: GlobalConfig.getHeaders(["JSON"]),
  url: GlobalConfig.getApiUrlFromRoot("/planets"),
  data
});

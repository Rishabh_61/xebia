import { combineReducers } from 'redux';
import HomePage from './HomePage';

const rootReducer = combineReducers({
  HomePage
});

export default rootReducer;
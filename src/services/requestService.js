import axios from "axios";
import HttpError from "standard-http-error";
import { get } from "lodash";
import { serialize, getLocalstorage } from "../helper";

export const successResponse = response => ({
  status: response.status,
  data: response.data
});

export const throwHttpError = error => {
  console.log(error);
  if (error.response.data) {
    throw new HttpError(
      get(error, "response.data.status.code"),
      get(error, "response.data.status.message"),
      {
        data: error.response.data
      }
    );
  }

  throw new HttpError(error.response.status, error.response.statusText);
};

export default async (request, httpService = axios) => {
  const { method, data, headers } = request;
  let { url } = request;
  console.log({ method, data, url, headers});
  const token = getLocalstorage("token");
  if (token) {
    headers.token = token;
  }
  if (method === "GET") {
    if (data) {
      url += `?${serialize(data)}`;
    }
  }
  return httpService
    .request({
      method,
      url,
      headers: Object.assign({}, headers),
      data
    })
    .then(successResponse, error => {
      throwHttpError(error);
    });
};

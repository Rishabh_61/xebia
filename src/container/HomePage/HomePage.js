import React, { Component } from 'react';
import './HomePage.css';
import { getLocalstorage } from '../../helper';
import { planetList } from '../../action/home.action';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Redirect } from 'react-router-dom'

class HomePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      search: '',
      redirect: false
    };
  }

  componentDidMount() {
    const isLogin = getLocalstorage('xebia_login');
    console.log(isLogin);
    if (!isLogin) {
      this.setState({
        redirect: true
      })
    }
  }

  onChange = (event) => {
    const value = event.target.value;
    this.setState({
      search: value
    })
  }

  onSubmit = async (event) => {
    this.props.planetListSubmit(undefined, [{search: this.state.search}])
  }
  render() {

    if (this.state.redirect) {
      return <Redirect to='/' />
    }
    return (
      <div>
        <div className="navbar">
          <input type="text" onChange = {this.onChange}></input>
          <input type="button" value={this.props.searchButtonText} onClick = {this.onSubmit}/>
        </div>
        {this.props.planetList? this.props.planetList.map((ele) => 
        {
          let height = ele.population.substring(0, 3);
          if (height < 20 || !height) {
            height =25;
          }
         return <div style = {{ background: 'wheat' , fontSize: '18px', border: '1px solid #333', height: `${height}px`}} key={ele.url}> <p>
           Name: {ele.name} | Population: {ele.population}</p> </div>})
    
        : null }

      </div>
    )
}
}

const mapStateToProps = state => {
  console.log(state)
  return {
    planetList: state.HomePage.planetList,
    searchButtonText: state.HomePage.searchButtonText
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    planetListSubmit: planetList
  }, dispatch);

};


export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
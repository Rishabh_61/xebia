import * as apiServices from "../services/apiService";
import request from "../services/requestService";

import { setLocalstorage } from "../helper";

export const userLogin = (requestMethod = request, params) => {
  const { username, password } = params;
  return requestMethod(apiServices.userLoginParams({search: username}))
    .then(login => {
      const { data } = login;
      const { results } = data;
      if (!results || !results.length ) {
        throw new Error('User Not Found!!!')
      };
      console.log(results);
      const userExist = results.filter((res) => {
        if (res.birth_year === password && res.name === username)
          return true;
        return false;
        
      });
      if (!userExist || !userExist.length) {
        throw new Error('Wrong Username or Password')
      }
      setLocalstorage("xebia_login", true);

      return true;
    })
    .catch(e => {
      console.log("Login Failed", e.message);
      throw e;
    });
};

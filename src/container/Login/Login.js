import React, { Component } from 'react';
import './Login.css';
import { userLogin } from "../../action/login.action";
import { Redirect } from 'react-router-dom'

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      buttonText: 'Sign In',
      btnStatus: false,
      redirect: false
    };
  }

  handleChange = event => {
    let { name, value } = event.target;
    this.setState({
      [name]: value
    })
  };

  onSubmit = async event => {
    event.preventDefault();
    const { username, password } = this.state;
    console.log({username, password})
    if (!username || !password) {
      alert('Please Provide Username and Password Both');
    }
    try {
      this.setState({
        buttonText: 'AUTHENTICATING...',
        btnStatus: true
      })
      await userLogin(undefined, {
        username,
        password
      });
      this.setState({
        redirect: true
      })
    } catch(e) {
      console.log(e);
      this.setState({
        btnStatus: false,
        buttonText: 'Sign In',
      })
      alert(e.message);
    }
  };
  render() {
    if (this.state.redirect === true) {
      return <Redirect to='/home' />
    }
    return (
      <div className="container">
        <h3>Star Wars</h3>
        <div className="form-group">
          <label htmlFor="username" id="username" >UserName</label>
          <input type="text" id="username" name="username" onChange={this.handleChange}/>
        </div>
        <div className="form-group">
          <label htmlFor="password" id="password">Password</label>
          <input type="password" id="password" name="password" onChange={this.handleChange}/>
        </div>
        <input className="btn" type="submit" value={this.state.buttonText} disabled = {this.state.btnStatus} onClick= {this.onSubmit}/>
      </div>
    );
  }
}

export default Login;
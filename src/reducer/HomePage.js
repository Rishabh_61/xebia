import { HOMEPAGE } from '../actionType';

const initialState = {
  planetList: [],
  searchButtonText: 'Search'
};

export default function getPlanetList(state = initialState, action) {
  const newState = { ...state };
  switch (action.type) {
    case HOMEPAGE.HOME_PAGE_SEARCH_START:
      return {
        ...state,
        searchButtonText: 'Searching'
      }
    case HOMEPAGE.HOME_PAGE_SEARCH_SUCCESS:
      return {
        ...state,
        planetList: action.payload,
        searchButtonText: 'Search'
      }
    default:
      return newState;
  }

}
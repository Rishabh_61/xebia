export const serialize = obj => {
  const str = [];
  const keys = Object.keys(obj);
  keys.forEach(key => {
    str.push(`${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`);
  });
  return str.join("&");
};

export const setLocalstorage = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
};

export const getLocalstorage = key => {
  return JSON.parse(localStorage.getItem(`${key}`) || null);
};

export const removeItemFromLocalStorage = key => {
  localStorage.removeItem(key);
};

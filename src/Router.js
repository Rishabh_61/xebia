import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './container/Login/Login';
import HomePage from './container/HomePage/HomePage'

const Router = () => (
  <Switch>
    <Route exact path = "/" component = {Login}></Route>
    <Route exact path = "/home" component = {HomePage}></Route>

    {/* <Route component={NotFound} /> */}
  </Switch>
);

export default Router;
import * as apiServices from "../services/apiService";
import request from "../services/requestService";
import { HOMEPAGE } from '../actionType';

export const planetList = (requestMethod = request, params) => {

  return function(dispatch) {
    dispatch({
      type: HOMEPAGE.HOME_PAGE_SEARCH_START,
    });
      return requestMethod(apiServices.planetsList(params))
    .then(planets => {
      const { data } = planets;
      const { results } = data;
      dispatch({
        type: HOMEPAGE.HOME_PAGE_SEARCH_SUCCESS,
        payload: results
    });
      return true;
    })
    .catch(e => {
      console.log("Login Failed", e.message);
      throw e;
    });
  };


};